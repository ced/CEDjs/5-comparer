/*
@licstart  The following is the entire license notice for the
JavaScript code in this page.

Copyright (C) 2020 Moulinux

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/
const DICO_FR = {
    'enTete1': 'Calculette Eco-déplacements',
    'enTete2': `Calculez l'impact de vos déplacements quotidiens
        sur l\'environnement et vos dépenses`,
    'distance': 'Quelle est la distance entre le domicile et le lieu de travail ?',
    'domicile1': 'J\'habite à ',
    'domicile2': ' km de mon travail.',
    'deplacement1': 'Choisir le ',
    'deplacement2': ` mode de déplacement :
        1 - train
        2 - vélo
        3 - voiture
        Mon choix :`,
    1: 'premier',
    2: 'second',
    'train': "le train",
    'velo': "le vélo",
    'voiture': "la voiture",
    'choix1': 'Je choisis ',
    'choix2': 'plutôt que ',
    'vide': 'saisir une valeur',
    'nombre': 'saisir un nombre',
    'haut': 'valeur entière trop importante',
    'bas': 'valeur entière trop basse',
};
const DICO_COUT = {
    'train': [14.62, 9.26],
    'velo': [0, 0],
    'voiture': [129.62, 50.65],
    'labTitre': ["Effet de serre", "Énergie"],
    'labUnite': [" kg eq. CO2", " l eq. pétrole"],
    'labEco1': ["J'évite ", "Je consomme "],
    'labEco2': [" kg eq. CO2 par an", " litres eq. pétrole en moins par an"],
    'labDep1': ["J'émets ", "Je consomme "],
    'labDep2': [" kg eq. CO2 en plus par an", " litres eq. pétrole en plus par an"]
};

/**
* Calcule les coûts de déplacement.
* 
* @param lesCouts liste contenant les éléments kilométriques liés à un mode de déplacement
* @param km distance entre le domicile et le lieu de travail
* @return liste contenant les coûts kilométriques liés à un mode de déplacement
*/
function calculeCoutKm(lesCouts, km) {
    let res = [];   // Initialiser la liste des résultats
    for (indice in lesCouts) {
        res[indice] = Math.round(lesCouts[indice]*km*100)/100;
    }
    return res;
}

/**
* Demande à l'utilisateur de saisir un entier.
* 
* @param message question posée à l'utilisateur
* @param limiteH limite supérieure entière pour la saisie
* @param limiteB limite inférieure entière pour la saisie
* @return un entier
*/
function choixEntier(message, limiteH, limiteB) {
    while (true) {
        var rep = prompt(message);
        try {
            if(rep == "") throw DICO_FR['vide'];
            if(isNaN(rep)) throw DICO_FR['nombre'];
            rep = parseInt(rep);
            if(rep < limiteB) throw DICO_FR['bas'];
            if(rep > limiteH) throw DICO_FR['haut'];
            break;
        }
        catch(err) {
            alert(err);
        }
    }
    return rep;
}

/**
* Demande à l'utilisateur de choisir un mode de déplacement.
* 
* @param limiteH limite supérieure entière pour la saisie
* @param limiteB limite inférieure entière pour la saisie
* @param dep entier représentant l'itération sur le mode de déplacement
* @param km entier représentant la distance entre le domicile et le lieu de travail
* @param choixUtilisateur tableau mémorisant les choix des l'utilisateur
* @return un tableau contenant les couts kilométriques liés au mode de déplacement
*/
function choixDeplacement(limiteH, limiteB, dep, km, choixUtilisateur) {
    let choix = choixEntier(DICO_FR['deplacement1'] + DICO_FR[dep] + DICO_FR['deplacement2'], limiteH, limiteB);
    let lesCoutsKm = [0, 0];
    switch (choix) {
        case 1:
            lesCoutsKm = calculeCoutKm(DICO_COUT['train'], km);
            choixUtilisateur[choixUtilisateur.length] = DICO_FR['train']
            break;
        case 2:
            lesCoutsKm = calculeCoutKm(DICO_COUT['velo'], km);
            choixUtilisateur[choixUtilisateur.length] = DICO_FR['velo']
            break;
        case 3:
            lesCoutsKm = calculeCoutKm(DICO_COUT['voiture'], km);
            choixUtilisateur[choixUtilisateur.length] = DICO_FR['voiture']
            break;
    }
    return lesCoutsKm;
}

/**
* Compare les coûts de déplacement.
* 
* @param lesCouts1 liste contenant les coûts calculés pour le premier mode de déplacement
* @param lesCouts2 liste contenant les coûts calculés pour le second mode de déplacement
* @return liste contenant les coûts kilométriques comparés
*/
function compareCoutKm(lesCouts1, lesCouts2) {
    let diff = [];
    for (indice in lesCouts1) {
        diff[indice] = Math.round((lesCouts2[indice]-lesCouts1[indice])*100)/100;
    }
    return diff;
}
